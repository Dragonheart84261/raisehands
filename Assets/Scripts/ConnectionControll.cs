﻿using System.Collections;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class ConnectionControll : Singleton<ConnectionControll>
{
    public void RaiseHand()
    {
        Instance.StartCoroutine(SendData());
    }

    public static IEnumerator SendData()
    {
        Debug.Log("Start Sending");

        WWWForm form = new WWWForm();

        //form.AddField("https://docs.google.com/spreadsheets/d/12A27-q1rBB9mIJAB4rw6vvU9b4XsS9l5pw7H930pVyM/edit#gid=0&range=A1", 12);

        form.AddField("https://docs.google.com/spreadsheets/d/12A27-q1rBB9mIJAB4rw6vvU9b4XsS9l5pw7H930pVyM/edit#gid=0&range=A1", "hello");

        using (UnityWebRequest www = UnityWebRequest.Post("https://docs.google.com/spreadsheets/d/12A27-q1rBB9mIJAB4rw6vvU9b4XsS9l5pw7H930pVyM/edit?usp=sharing", form))
        {
            yield return www.SendWebRequest();

            if (www.isNetworkError)
            {
                Debug.LogError("WARNING Network FAILURE");

            }
            else if (www.isHttpError)
            {
                Debug.LogError("WARNING HTTP FAILURE");
            }
            else
            {
                Debug.Log("End Sending");
            }
        }
    }
}
