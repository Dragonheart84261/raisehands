﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Android;

public class ProgrammState : Singleton<ProgrammState>
{
    private Status currentStatus;

    private bool permissionGranted = false;

    public static bool PermissionGranted { get => Instance.permissionGranted;}

    void Start()
    {
        currentStatus = Status.Init;
        TryToChangeStatus(Status.Login);        
    }

    public static void TryToChangeStatus(Status statusChangeTo)
    {
        if(Instance.currentStatus != statusChangeTo)
        {
            Instance.permissionGranted = true;
        }

        switch(statusChangeTo)
        {
            case Status.Login:
                {
                    if(PermissionGranted)
                    {
                        Instance.currentStatus = statusChangeTo;
                        SceneChange.ChangeToLogin();
                    }
                    else
                    {
                        Debug.LogError("You are in Login");
                    }
                    break;
                }
            case Status.Room:
                {
                    if(PermissionGranted)
                    {
                        Instance.currentStatus = statusChangeTo;
                        SceneChange.ChangeToRaiseHands();
                    }
                    else
                    {
                        Debug.LogError("You are in Room");
                    }
                    break;
                }
            default:
                {
                    Debug.LogError("ERROR NO STATUS");
                    break;
                }
        }
    }
}

public enum Status
{
    Init,
    Login,
    Room
}
