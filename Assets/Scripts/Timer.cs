﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Timer : MonoBehaviour
{
    float refreshTime;
    float currentTime;

    public float CurrentTime { get => currentTime;}

    void Start()
    {
        refreshTime = 5;
        currentTime = refreshTime;
    }

    public void Tick()
    {
        currentTime -= Time.deltaTime;
    }
}
