﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneChange : Singleton<SceneChange>
{
    public static void ChangeToLogin()
    {
        SceneManager.LoadScene("Login");
    }

    public static void ChangeToRaiseHands()
    {
        SceneManager.LoadScene("RaiseHand");
    }
}
