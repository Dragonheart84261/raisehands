﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InfoContainer : Singleton<InfoContainer>
{
    string nameContainer;
    string linkContainer;

    public static void SaveInContainer(string name, string link)
    {
        Instance.nameContainer = name;
        Instance.linkContainer = link;
        Debug.Log(link);
    }

    public static string ReturnName()
    {
        return Instance.nameContainer;
    }

    public static string ReturnLink()
    {
        return Instance.linkContainer;
    }
}
