﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Login : MonoBehaviour
{
    [SerializeField] InputField iNameField;
    [SerializeField] InputField iLinkField;

    public void SaveStrings()
    {
        InfoContainer.SaveInContainer(iNameField.text, iLinkField.text);
        ProgrammState.TryToChangeStatus(Status.Room);
    }
}
